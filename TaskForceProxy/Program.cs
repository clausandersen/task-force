using ClausAndersen.TaskForce.Apis;
using System;
using System.IO.Pipes;

namespace ClausAndersen.TaskForce.Proxy
{
	class Program
	{
		static int Main(string[] args)
		{
			var exitCode = 0;

			using (var clientStream = new NamedPipeClientStream(".", "TaskForce", PipeDirection.InOut, PipeOptions.Asynchronous))
			{
				clientStream.Connect(1000);
				clientStream.ReadMode = PipeTransmissionMode.Message;

				var localApi = new LocalApiProxy(clientStream);

				try
				{
					Console.WriteLine($"Length of '{args[0]}' is {localApi.GetLength(args[0]).Result}.");
					//localApi.HelloAsync(args[0]).Wait();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex);
					exitCode = 1;
				}
			}

			return exitCode;
		}
	}
}
