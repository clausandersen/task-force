﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce
{
	public abstract class MessagePassingObject
	{
		readonly Stream stream;
		//readonly byte[] buffer = new byte[64 * 1024];

		protected MessagePassingObject(Stream stream)
		{
			this.stream = stream;
		}

		protected async Task SendMessageAsync(Message message)
		{
			var bytes = message.Serialize();
			Console.WriteLine($"Sending message of {bytes.Length} bytes.");
			await stream.WriteAsync(bytes, 0, bytes.Length);
		}

		protected async Task<Message> ReceiveMessageAsync()
		{
			byte[] buffer = new byte[64 * 1024];
			var length = await stream.ReadAsync(buffer, 0, buffer.Length);
			if (length == 0)
			{
				throw new Exception("Stream died.");
			}
			Console.WriteLine($"Received message of {length} bytes.");
			return Message.Deserialize(buffer, length);
		}
	}
}
