﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace ClausAndersen.TaskForce
{
	public class IPAddress2
	{
		static UnicastIPAddressInformation GetLocalAddressAndSubnetMask()
		{
			var localIPAddress = GetLocalIPAddress();

			foreach (var inf in NetworkInterface.GetAllNetworkInterfaces())
			{
				if (inf.NetworkInterfaceType != NetworkInterfaceType.Loopback && inf.OperationalStatus == OperationalStatus.Up)
				{
					foreach (var unicastAddress in inf.GetIPProperties().UnicastAddresses)
					{
						if (unicastAddress.Address.AddressFamily == AddressFamily.InterNetwork)
						{
							if (unicastAddress.Address.Address == localIPAddress.Address)
							{
								return unicastAddress;
							}
						}
					}
				}
			}

			return null;
		}

		static IPAddress GetLocalIPAddress()
		{
			using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
			{
				socket.Connect("8.8.8.8", 65530);
				var endPoint = socket.LocalEndPoint as IPEndPoint;
				return endPoint.Address;
			}
		}

		public static IPAddress GetSubnetBroadcastAddress()
		{
			var info = GetLocalAddressAndSubnetMask();
			var addressBytes = info.Address.GetAddressBytes();
			var maskBytes = info.IPv4Mask.GetAddressBytes();

			var bytes = new byte[4];
			for (int i = 0; i < 4; i++)
			{
				bytes[i] = (byte)(addressBytes[i] & maskBytes[i]);
				bytes[i] = (byte)(bytes[i] | ~maskBytes[i]);
			}

			return new IPAddress(bytes);
		}

		public IPAddress Address { get; set; }
		public IPAddress SubnetMask { get; set; }

		public IPAddress2() { }
	}
}
