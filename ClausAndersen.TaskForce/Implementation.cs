﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce
{
	public abstract class Implementation : MessagePassingObject
	{
		readonly Dictionary<string, MethodInfo> methods = new Dictionary<string, MethodInfo>();

		protected Implementation(Stream stream) : base(stream)
		{
			foreach (var method in GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public))
			{
				methods[method.Name] = method;
			}
		}

		public async Task RunAsync()
		{
			try
			{
				while (true)
				{
					var requestMessage = await ReceiveMessageAsync();
					switch (requestMessage.Type)
					{
						case MessageType.Request:
							if (methods.TryGetValue(requestMessage.Name, out var methodInfo))
							{
								var responseMessage = new Message
								{
									Id = requestMessage.Id,
									Name = requestMessage.Name
								};

								try
								{
									if (methodInfo.ReturnType == typeof(Task))
									{
										await (Task)methodInfo.Invoke(this, requestMessage.Arguments);
									}
									else
									{
										var returnValue = await (Task<object>)methodInfo.Invoke(this, requestMessage.Arguments);
										responseMessage.Arguments = new[] { returnValue };
									}
									responseMessage.Type = MessageType.Response;
								}
								catch (Exception ex)
								{
									responseMessage.Type = MessageType.Exception;
									responseMessage.Exception = ex;
								}
								await SendMessageAsync(responseMessage);
							}
							else
							{
								var responseMessage = new Message
								{
									Id = requestMessage.Id,
									Name = requestMessage.Name,
									Type = MessageType.Exception,
									Exception = new NotImplementedException()
								};
								await SendMessageAsync(responseMessage);
							}
							break;
						case MessageType.Response:
							break;
						case MessageType.Exception:
							break;
						case MessageType.StdOut:
							break;
						case MessageType.StdError:
							break;
						case MessageType.StdIn:
							break;
						default:
							break;
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		protected Task ConsoleWrite(string text)
		{
			return SendMessageAsync(new Message { Type = MessageType.StdOut, Arguments = new[] { text } });
		}

		protected Task ConsoleWriteLine(string text)
		{
			return SendMessageAsync(new Message { Type = MessageType.StdOut, Arguments = new[] { text + "\n" } });
		}
	}
}
