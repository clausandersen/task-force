﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace ClausAndersen.TaskForce
{
	public class Message
	{
		public static Message Deserialize(byte[] bytes)
		{
			return Deserialize(bytes, bytes.Length);
		}

		public static Message Deserialize(byte[] bytes, int length)
		{
			var json = Encoding.UTF8.GetString(bytes, 0, length);
			try
			{
				return JsonConvert.DeserializeObject<Message>(json);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public int Id { get; set; }
		public MessageType Type { get; set; }
		public string Name { get; set; }
		public object[] Arguments { get; set; }
		public Exception Exception { get; set; }

		public byte[] Serialize()
		{
			var json = JsonConvert.SerializeObject(this);
			return Encoding.UTF8.GetBytes(json);
		}
	}
}
