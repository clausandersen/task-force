﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce.Apis
{
	public class LocalApiImplementation : Implementation, ILocalApi
	{
		public LocalApiImplementation(Stream stream) : base(stream)
		{ }

		public Task HelloAsync(string name)
		{
			Console.WriteLine($"Hello to {name}.");

			if (name == "a")
			{
				throw new Exception("Bad name!");
			}

			if (name == "b")
			{
				ConsoleWriteLine("Is your name really b?");
			}

			return Task.CompletedTask;
		}

		public Task<int> GetLength(string text)
		{
			return Task.FromResult(text.Length);
		}
	}
}
