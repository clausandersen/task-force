﻿using System.Threading.Tasks;

namespace ClausAndersen.TaskForce.Apis
{
	public interface ILocalApi
	{
		Task HelloAsync(string name);
		Task<int> GetLength(string text);
	}
}
