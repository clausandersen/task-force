﻿using System.IO;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce.Apis
{
	public class LocalApiProxy : Proxy, ILocalApi
	{
		public LocalApiProxy(Stream stream) : base(stream)
		{ }

		public async Task HelloAsync(string name)
		{
			await CallRemoteFunctionAsync(nameof(HelloAsync), name);
		}

		public async Task<int> GetLength(string text)
		{
			return await CallRemoteFunctionAsync<int>(nameof(GetLength), text);
		}
	}
}
