﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce
{
	public abstract class Proxy : MessagePassingObject
	{
		int messageId = 1;

		protected Proxy(Stream stream) : base(stream)
		{ }

		protected async Task CallRemoteFunctionAsync(string methodName, params object[] arguments)
		{
			var requestMessage = new Message { Id = messageId++, Name = methodName, Arguments = arguments, Type = MessageType.Request };
			await SendMessageAsync(requestMessage);

			while (true)
			{
				var responseMessage = await ReceiveMessageAsync();

				switch (responseMessage.Type)
				{
					case MessageType.Request:
						break;
					case MessageType.Response:
						return;
					case MessageType.Exception:
						throw responseMessage.Exception;
					case MessageType.StdOut:
						Console.Out.Write(responseMessage.Arguments[0]);
						break;
					case MessageType.StdError:
						Console.Error.Write(responseMessage.Arguments[0]);
						break;
					case MessageType.StdIn:
						break;
					default:
						break;
				}
			}
		}

		protected async Task<T> CallRemoteFunctionAsync<T>(string methodName, params object[] arguments)
		{
			var requestMessage = new Message { Id = messageId++, Name = methodName, Arguments = arguments, Type = MessageType.Request };
			await SendMessageAsync(requestMessage);

			while (true)
			{
				var responseMessage = await ReceiveMessageAsync();

				switch (responseMessage.Type)
				{
					case MessageType.Request:
						break;
					case MessageType.Response:
						return (T)responseMessage.Arguments.Single();
					case MessageType.Exception:
						throw responseMessage.Exception;
					case MessageType.StdOut:
						Console.Out.Write(responseMessage.Arguments[0]);
						break;
					case MessageType.StdError:
						Console.Error.Write(responseMessage.Arguments[0]);
						break;
					case MessageType.StdIn:
						break;
					default:
						break;
				}
			}
		}
	}
}
