﻿namespace ClausAndersen.TaskForce
{
	public enum MessageType
	{
		Request,
		Response,
		Exception,
		StdOut,
		StdError,
		StdIn
	}
}
