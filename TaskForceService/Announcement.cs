﻿using Newtonsoft.Json;
using System.Text;

namespace ClausAndersen.TaskForce.Service
{
	class Announcement
	{
		public static Announcement Deserialize(byte[] bytes)
		{
			return Deserialize(bytes, bytes.Length);
		}

		public static Announcement Deserialize(byte[] bytes, int length)
		{
			var json = Encoding.UTF8.GetString(bytes, 0, length);
			return JsonConvert.DeserializeObject<Announcement>(json);
		}

		public string MachineName { get; set; }
		public string UserName { get; set; }

		public override string ToString()
		{
			return $"MachineName={MachineName} UserName={UserName}";
		}

		public byte[] Serialize()
		{
			var json = JsonConvert.SerializeObject(this);
			return Encoding.UTF8.GetBytes(json);
		}
	}
}
