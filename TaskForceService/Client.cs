using System;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce.Service
{
	class Client
	{
		readonly TcpClient tcpClient;
		readonly NetworkStream tcpStream;
		Task task;

		public Client(TcpClient tcpClient)
		{
			this.tcpClient = tcpClient;
			tcpStream = tcpClient.GetStream();
		}

		async Task SendMessageAsync(Message message)
		{
			var bytes = message.Serialize();
			var messageSize = bytes.Length;
			var messageSizeBytes = BitConverter.GetBytes(messageSize);
			await tcpStream.WriteAsync(messageSizeBytes, 0, messageSizeBytes.Length);
			await tcpStream.WriteAsync(bytes, 0, bytes.Length);
		}

		public void Run()
		{
			task = Task.Run(async () =>
			{
				var buffer = new byte[64 * 1024];
				while (true)
				{
					var length = await tcpStream.ReadAsync(buffer, 0, 4);
					if (length != 4)
					{
						break;
					}
					var messageSize = BitConverter.ToInt32(buffer, 0);
					if (messageSize > buffer.Length)
					{
						break;
					}
					length = await tcpStream.ReadAsync(buffer, 0, messageSize);
					if (length != messageSize)
					{
						break;
					}
					var message = Message.Deserialize(buffer, messageSize);
					Console.WriteLine(message);
					await HandleMessageAsync(message);
				}
			});
		}

		async Task HandleMessageAsync(Message message)
		{
			switch (message.Type)
			{
				case MessageType.Request:
					await SendMessageAsync(new Message { Id = message.Id, Name = message.Name, Type = MessageType.Response });
					break;

				case MessageType.Response:
					break;

				case MessageType.Exception:
					break;

				default:
					break;
			}
		}
	}
}
