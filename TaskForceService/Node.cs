﻿using System;
using System.Net;

namespace ClausAndersen.TaskForce.Service
{
	class Node
	{
		public string MachineName { get; set; }
		public IPAddress IPAddress { get; set; }
		public DateTime LastSeen { get; set; }
	}
}
