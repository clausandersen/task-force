using ClausAndersen.TaskForce.Apis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClausAndersen.TaskForce.Service
{
	class Program
	{
		const int discoveryPort = 33000;
		const int commandPort = 33001;

		static readonly List<Node> nodes = new List<Node>();
		static readonly List<Client> clients = new List<Client>();

		static void Main(string[] args)
		{
			var tcpListener = new TcpListener(IPAddress.Any, commandPort);

			var commandTask = Task.Run(async () =>
			{
				while (true)
				{
					AcceptClient(await tcpListener.AcceptTcpClientAsync());
				}
			});

			var udpClient = new UdpClient(discoveryPort);
			var broadcastEndPoint = new IPEndPoint(IPAddress2.GetSubnetBroadcastAddress(), discoveryPort);

			var announcementTask = Task.Run(async () =>
			{
				var announcement = new Announcement { MachineName = Environment.MachineName, UserName = Environment.UserName };
				var announcementString = JsonConvert.SerializeObject(announcement);
				var announcementBytes = Encoding.UTF8.GetBytes(announcementString);
				while (true)
				{
					await udpClient.SendAsync(announcementBytes, announcementBytes.Length, broadcastEndPoint);
					await Task.Delay(1000);
				}
			});

			var discoveryTask = Task.Run(async () =>
			{
				while (true)
				{
					var udpReceiveResult = await udpClient.ReceiveAsync();
					var announcement = Announcement.Deserialize(udpReceiveResult.Buffer);
					Console.WriteLine($"Announcement received: {announcement}");

					var node = (from n in nodes where n.IPAddress.Address == udpReceiveResult.RemoteEndPoint.Address.Address && n.MachineName == announcement.MachineName select n).FirstOrDefault();
					if (node == null)
					{
						node = new Node { IPAddress = udpReceiveResult.RemoteEndPoint.Address, MachineName = announcement.MachineName };
						nodes.Add(node);
						Console.WriteLine($"New node '{node.MachineName}' found at IP {node.IPAddress}.");
						CreateClientTask(node);
					}
					node.LastSeen = DateTime.UtcNow;
				}
			});

			var pipeServerTask = Task.Run(async () =>
			{
				var pipeServer = new NamedPipeServerStream("TaskForce", PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous)
				{
					ReadMode = PipeTransmissionMode.Message
				};

				while (true)
				{
					await pipeServer.WaitForConnectionAsync();
					Console.WriteLine("Client connected through pipe.");

					var localApiImplementation = new LocalApiImplementation(pipeServer);
					await localApiImplementation.RunAsync();

					pipeServer.Disconnect();
					Console.WriteLine("Client connected through pipe disconnected.");
				}
			});

			while (true)
			{
				Thread.Sleep(100);
			}
		}

		static void CreateClientTask(Node node)
		{
			Task.Run(() =>
			{

			});
		}

		static void AcceptClient(TcpClient tcpClient)
		{
			var client = new Client(tcpClient);
			clients.Add(client);
			client.Run();
		}
	}
}
